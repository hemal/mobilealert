﻿/********************************************************************
	created:	2015/04/06
	created:	6:4:2015   15:01
	filename: 	c:\projects\mobilealert\mobilealert\mobilealertmaindlg.cs
	file path:	c:\projects\mobilealert\mobilealert
	file base:	mobilealertmaindlg
	file ext:	cs
	author:		vhn
	
	purpose:	One file implementation of mobileAlert, for a small app
                app like this using seperate threads would be way to 
                expensive (about 1MB for every thread) so Thread.Sleep
                has been called in the main thread. 
                As serial communication is rather slow, response time
                and messages from the modem take time, the calls to the
                device itself takes seconds but commands to the operator
                like USSD, balance via SMS and SMS response take time
                from 3s to 30s depending on what is needed (see telit
                doc's for detailed explanation) That is why thread timers
                are nessesary just to make sure we get what we requested.
                When the application beings, the modem is initialized via
                CONNECT button when truns GREEN to signal that the port is
                open, then the simplest AT command is sent to initialize
                and make sure everything is fine - OK message is needed.
                Then AT+CLIP is sent to make sure we get the number of the
                incomming caller, then AT+CMGF=1 is sent to enable us to
                make life easier and sent text messages
                DATARECEIVED event is triggred when something appears in 
                the buffer, this event is buggy (SEE MSDN forums) which
                sometimes tends not to notice when the buffer has got 
                data (threads are helpful here) so the ReadExisting() 
                function was not used, instead the readline works fine.
                
                TODO:
                1. Add a gird to show when a call was received and SMS sent
                2. Get rid of the LOG - for development purposes only
                3. Use XML to store values
                4. Use backgroundworker() to get rid of timers in the main
                   thread
                5. Add tectChanged event
    
     NOTE:      Commit any changes to developer branch first, then merge on 
                release
*********************************************************************/

using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;

namespace MobileAlert
{   
    public partial class MobileAlertMainDlg : Form
    {

        //////////////////////////////////////////////////////////////////////////////////////////////////
        // Class properties 
        private string      _inputData       = string.Empty;     // Kinda buffer of messages received from the modem
        private string[]    _phoneNumbers;                       // Alert message send to numbers specified
        private string      _alertMessage    = string.Empty;     // One alert message
        

        //////////////////////////////////////////////////////////////////////////////////////////////////
        // Class methods  
        
        /// <summary>
        /// Send Text Alert
        /// </summary>
        /// <param name="incommingNumber"></param>
        private void SendAlertMessage(string incommingNumber)
        {
            string strAlertMessage;
            List<string> numbersList = _phoneNumbers.ToList();
            numbersList.Add(incommingNumber);

            // FOOL proof from someone who alters the text and ommits {}
            if (_alertMessage.Contains("{0}"))
                strAlertMessage = string.Format(_alertMessage, incommingNumber);
            else
                strAlertMessage = _alertMessage;

            foreach (string strNumber in numbersList)
            {
                txtLog.AppendText("\n Sending message to: " + strNumber + Environment.NewLine);
                Thread.Sleep(5000); // 2 Second mighty sleep to make it right
                // Prepare to whom the message is to be sent and goto new line
                serialPort.Write("AT+CMGS="+ strNumber + Environment.NewLine);
                Thread.Sleep(200);
                // > appears and message is written - ctrl-z to end (char 26)
                serialPort.Write(strAlertMessage + char.ConvertFromUtf32(26) + Environment.NewLine);
                Thread.Sleep(2000);
            }
        }

        /// <summary>
        /// Callback when a receive event is detected
        /// </summary>
        /// <param name="text"></param>
        private delegate void SetModemCallBack(string text);

        /// <summary>
        ///  Set text from call back
        /// </summary>
        /// <param name="text"></param>
        private void SetText(string text)
        {
            //Mobile Number
            txtLog.AppendText(text);
            //serialPort.rea
            if (text.Contains(@"+CLIP:"))
            {
                // Hang up call
                serialPort.Write("ATH\r");
       
                // Determine caller's number
                Regex incommingCallerRegex = new Regex("\"[^\"]*\"");
                Match incommingCallerMatch = incommingCallerRegex.Match(text);
                
                SendAlertMessage(incommingCallerMatch.ToString());
            }

            if (text.Contains(@"Balance"))
            {
                Regex balanceRegex = new Regex("(?<=Balance :).*?(?=,)");
                Match balanceMatch = balanceRegex.Match(text);
                txtBalance.Text = balanceMatch.ToString();
            }
            if (text.Contains(@"number"))
            {
                Regex cellNumber =  new Regex("(?<=number is ).*?(?=.\")");
                Match cellNumberMatch = cellNumber.Match(text);
                txtCellNumber.Text = cellNumberMatch.ToString();
            }

            if (text.Contains(@"+COPS: 0,0"))
            {
                Regex cellOperator = new Regex("\"[^\"]*\"");
                Match cellOperatorMatch = cellOperator.Match(text);
                txtOperator.Text = cellOperatorMatch.ToString();
            }
        }

        /// <summary>
        /// Standard receive event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="serialDataReceived"></param>
        private void PortDataReceived(object sender, SerialDataReceivedEventArgs serialDataReceived)
        {
            // MSDN recommends
            SerialPort sp = (SerialPort) sender;
            //sp.ReadTimeout = 50000; // Nessary when using ReadLine() opposed to ReadExisting()
            _inputData = sp.ReadLine();
            if (_inputData != string.Empty)
                BeginInvoke(new SetModemCallBack(SetText), new object[] {_inputData});
        }

        /// <summary>
        /// Initialize
        /// </summary>
        public MobileAlertMainDlg()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Close the app - make sure port is closed too
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            if (serialPort.IsOpen)
            {
                serialPort.Close();
                Close();
            }
            else
                Close();
        }

        /// <summary>
        /// Open COM port and initialize
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnConnect_Click(object sender, EventArgs e)
        {
            if (btnConnect.Text == @"CONNECT")
            {
                if (txtPORT.TextLength > 2)
                {
                    serialPort.PortName     = txtPORT.Text;
                    
                    // These values are recomended by the mfg - check doc
                    serialPort.BaudRate     = 115200;
                    serialPort.Parity       = Parity.None;
                    serialPort.DataBits     = 8;
                    serialPort.StopBits     = StopBits.One;
                    serialPort.WriteTimeout = 1000;
                    serialPort.DtrEnable    = true;
                    serialPort.RtsEnable    = true;
                    
                    // Get recipients numbers and custom message
                    // Dirty way to read data in textfields when changed - will modify
                    _phoneNumbers = txtRecepients.Lines;
                    _alertMessage = txtAlertMessage.Text;
                    
                    try
                    {
                        // Get ready to receive data MSDN way
                        serialPort.DataReceived += PortDataReceived;

                        serialPort.Open();
                        serialPort.Handshake    = Handshake.None;                  // Modem GT864-PY does not support
                        btnConnect.Text         = @"DISCONNECT";
                        btnConnect.BackColor    = Color.Green;
                        txtLog.Enabled          = true;
                        txtATCommand.Enabled    = true;
                        btnSend.Enabled         = true;
                        
                        // Initial modem settings and info catcher
                        serialPort.WriteLine("AT" + (char)13);                      // Initialize modem
                        Thread.Sleep(100);
                        serialPort.WriteLine("AT+CLIP=1" + (char)13);               // Detect incmiiming number
                        Thread.Sleep(100);
                        serialPort.WriteLine("AT+COPS?" + (char)13);                // Get mobile operator
                        Thread.Sleep(100);
                        serialPort.WriteLine("AT+CUSD=1,*100*1*1#" + (char)13);     // Get balance (universal in Kenya)  
                        Thread.Sleep(4000);
                        serialPort.WriteLine("AT+CUSD=1,*100*1*5#" + (char)13);     // Get cell number (universal in Kenya)
                        Thread.Sleep(4000);
                    }
                    catch (Exception exception)
                    {
                        MessageBox.Show(exception.Message);
                    }
                }
            }

            // Make UI elegant, disable buttons and txt fields, change colors
            else
            {
                if (serialPort.IsOpen)
                {
                    serialPort.Close();
                    btnConnect.Text = @"CONNECT";
                    btnConnect.BackColor = Color.Red;
                    txtLog.Clear();
                    txtCellNumber.Clear();
                    txtOperator.Clear();
                    txtBalance.Clear();
                    txtLog.Enabled = false;
                    txtATCommand.Enabled = false;
                    btnSend.Enabled = false;
                }
            }
        }

        /// <summary>
        ///  Send a user AT Command, like top up using USSD etc 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSend_Click(object sender, EventArgs e)
        {
            if (txtATCommand.TextLength > 1)
            {
                serialPort.Write(txtATCommand.Text + "\r\n");
                Thread.Sleep(100);
            }
            else
            {
                MessageBox.Show(@"Please input AT COMMAND");
            }
        }

    }
}